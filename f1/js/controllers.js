'use strict';

/* Controllers */

var phonecatControllers = angular.module('phonecatControllers', []);

phonecatControllers.controller('PhoneListCtrl', ['$scope', 'Phone','$http',
  function($scope, Phone,$http) {
        if ($scope.$parent.history instanceof Array) {
		$scope.historys = $scope.$parent.history;
	} else {
		$scope.historys = [];
	}
	
	$scope.eventHandler = {
		addToHistorys : function(phone) {
			var list = $scope.historys;
			for ( var i = 0; i < list.length; i++) {
				if (list[i].id === phone.id) {
					list.splice(i, 1);
				}
			}
			$scope.historys.unshift(phone);
		}
	};
	$scope.$parent.history = $scope.historys;
    var ori;
    $http.get('phones/phones.json')
      .success(function(data) {
        $scope.phones=data;
    angular.forEach($scope.phones, function(phone){
      console.log(phone.id);
    $http.get('phones/'+phone.id+'.json').success( function(res) {
    console.log("hjbh");
    phone.talkTime=parseInt(res.battery.talkTime||0);
    phone.weight=parseFloat(res.sizeAndWeight.weight||0);
    phone.screenSize=parseFloat(res.display.screenSize||0);
    phone.standbyTime=parseInt(res.battery.standbyTime||0);
    console.log(phone);
    
    });
    });
    console.log($scope.phones);
    
    });
    this.$inject = [ '$scope', 'Phone'];
  
  }]);

phonecatControllers.controller('PhoneCompareCtrl', ['$scope', '$routeParams', 'Phone',
  function($scope, $routeParams, Phone) {
    $scope.p1 = Phone.get({phoneId: $routeParams.phoneId1});
    $scope.p2 = Phone.get({phoneId: $routeParams.phoneId2}); 
  }
  ]);

phonecatControllers.controller('PhoneDetailCtrl', ['$scope', '$routeParams', 'Phone',
  function($scope, $routeParams, Phone) {
    $scope.phone = Phone.get({phoneId: $routeParams.phoneId}, function(phone) {
      $scope.mainImageUrl = phone.images[0];
    });

    $scope.setImage = function(imageUrl) {
      $scope.mainImageUrl = imageUrl;
    }
  }]);
