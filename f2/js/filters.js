'use strict';

/* Filters */
angular.module('notAvilable', []).filter('empty', function() {
  return function(input) {
    if (input == ""){
      return "Not Available";
    } else{
      return input;
    }
  };
});
angular.module('phonecatFilters', []).filter('checkmark', function() {
  return function(input) {
    return input ? '\u2713' : '\u2718';
  };
});
